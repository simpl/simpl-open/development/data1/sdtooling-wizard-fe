import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
  selector: '[appTrim]',
})
export class TrimDirective {
@Output() trimmed = new EventEmitter<string>();

  constructor(private el: ElementRef) {}

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    this.trim();
  } 

  @HostListener('paste', ['$event'])
  onPaste(event: ClipboardEvent) {
    this.trim();
  }

  @HostListener('blur', ['$event'])
  onBlur(event: any) {
    this.trim();
  }

  trim(): void {
    this.el.nativeElement.value = this.el.nativeElement.value.trim();
    this.trimmed.emit( this.el.nativeElement.value);
  }
}
