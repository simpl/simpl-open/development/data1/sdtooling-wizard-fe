import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-back-btn',
  templateUrl: './back-btn.component.html',
  styleUrls: ['./back-btn.component.scss'],
})
export class BackBtnComponent implements OnInit {
  constructor(private location: Location, public router: Router) {}

  ngOnInit(): void {}
  goBack() {
    console.log('Sono a pagina: ' + this.router.url)
    if (this.router.url == '/select-file') {
      this.router.navigateByUrl('/');
    }
    if (this.router.url == '/form') {
      this.router.navigateByUrl('/select-file');
    }
  }
}
