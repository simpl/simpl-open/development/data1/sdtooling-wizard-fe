# Gaia-X SD Creation Wizard

This project is a fork of the [Gaia-X SD Creation Wizard|https://gitlab.eclipse.org/eclipse/xfsc/self-description-tooling/sd-creation-wizard-frontend]

## End of Life Notice

**IMPORTANT: This project fork is no longer maintained.**

As of 2024-10-24, this repository has reached its end of life and will no longer receive updates or support.

### Replacements

The functionality of this application in project SIMPL has been moved to a new application, where development is going forward:

- [SIMPL SD UI](https://code.europa.eu/simpl/simpl-open/development/gaia-x-edc/simpl-sd-ui)